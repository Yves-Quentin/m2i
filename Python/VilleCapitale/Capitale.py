from Ville import Ville
class Capitale(Ville):
    def __init__(self, nom,pays=None,nbHabitants=0):
        super().__init__(nom,nbHabitants)
        self.pays = pays

    def get_Nom(self):
        return self.nom

    def set_Nom(self, nom):
        self.nom = nom

    def get_NbHabitants(self):
        if self.nbHabitants == None:
            return 0
        else:
            return self.nbHabitants

    def set_NbHabitants(self, nbHabitants):
        if self.nbHabitants < 0:
            self.nbHabitants = 0
        else:
            self.nbHabitants = nbHabitants

    def get_Pays(self):
        return self.pays

    def set_Pays(self, pays):
        self.pays = pays

    def __str__(self):
        return super().__str__() + " du pays " + self.pays

    def categorie(self):
        return "C"