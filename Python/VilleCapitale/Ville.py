class Ville:
    def __init__(self,nom,nbHabitants=0):
        self.nom = nom
        self.nbHabitants = nbHabitants

    def get_Nom(self):
        return self.nom

    def set_Nom(self,nom):
        self.nom = nom

    def get_NbHabitants(self):
        if self.nbHabitants == None:
            self.nbHabitants = 0
        else:
            return self.nbHabitants

    def set_NbHabitants(self, nbHabitants):
        if self.nbHabitants < 0:
            self.nbHabitants = 0
        else:
            self.nbHabitants = nbHabitants

    def nbHabitantsConnu(self):
        if self.nbHabitants > 0:
            return [self.nbHabitants, True]
        else:
            return [print("Le nombre d'habitant faux. "), False]

    def __str__(self):
        return "Nom : " + str(self.nom) +  " Nombre Habitants: " + str(self.nbHabitants)

    def categorie(self):
        if self.nbHabitants == 0:
            return "?"
        if self.nbHabitants < 500000:
            return "A"
        if self.nbHabitants > 500000:
            return "B"



