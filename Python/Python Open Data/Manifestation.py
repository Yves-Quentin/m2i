class Manifestation:
    def __init__(self,nomManifestation,codePostal,siteWeb,dateDebut,dateFin,communePrincipal,listDomaine):
        self.nomManifestation = nomManifestation
        self.codePostal = codePostal
        self.siteWeb = siteWeb
        self.dateDebut = dateDebut
        self.dateFin = dateFin
        self.communePrincipal = communePrincipal
        self.listDomaine = listDomaine

    def get_NomManifestation(self):
        return self.nomManifestation

    def set_NomManifestation(self,nomManifestation):
        self.nomManifestation = nomManifestation

    def get_CodePostal(self):
        return self.codePostal

    def set_CodePostal(self,codePostal):
        self.codePostal = codePostal

    def get_SiteWeb(self):
        return self.siteWeb

    def set_SiteWeb(self,siteWeb):
        self.siteWeb = siteWeb

    def get_DateDebut(self):
        return self.dateDebut

    def set_DateDebut(self,dateDebut):
        self.dateDebut = dateDebut

    def get_DateFin(self):
        return self.dateFin

    def set_DateFin(self,dateFin):
        self.dateFin = dateFin

    def get_CommunePrincipal(self):
        return self.communePrincipal

    def set_CommunePrincipal(self,communePrincipal):
        self.communePrincipal = communePrincipal

    def get_ListDomaine(self):
        return self.listDomaine

    def set_ListDomaine(self,listDomaine):
        self.listDomaine = listDomaine

    def __str__(self):
        return "Nom de la manifestation : " + str(self.nomManifestation) + " Code postal :  " + str(self.codePostal) + " Site web :  " + str(self.siteWeb) \
               + " Date de début :  " + str(self.dateDebut) + " Date de fin :  " + str(self.dateFin) + " Commune principal :  " + str(self.communePrincipal) \
               + " Liste de domaine :  " + str(self.listDomaine)





